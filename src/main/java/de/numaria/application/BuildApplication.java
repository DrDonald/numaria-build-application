package de.numaria.application;

import de.numaria.application.command.BuildCommand;
import de.numaria.application.config.IConfiguration;
import de.numaria.application.config.ModelBuildApplicationConfig;
import de.numaria.application.config.impl.JsonConfiguration;
import de.numaria.application.listener.BuildSystemListener;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * @author Jasper Stritzke
 * @since 02.08.21
 */
@Getter
public class BuildApplication extends JavaPlugin {

    private IConfiguration<ModelBuildApplicationConfig> configuration;

    @Override
    public void onEnable() {
        this.configuration = new JsonConfiguration<>(
                new File("plugins//BuildSystem//config.json"),
                ModelBuildApplicationConfig.class
        ).createIfNotExists(ModelBuildApplicationConfig::new)
                .load().replaceKeysLocalGet("%prefix%", ModelBuildApplicationConfig::getPrefix);

        this.getCommand("build")
                .setExecutor(new BuildCommand(this));

        this.getServer()
                .getPluginManager()
                .registerEvents(new BuildSystemListener(this), this);
    }
}
