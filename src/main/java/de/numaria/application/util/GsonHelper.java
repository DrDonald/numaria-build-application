package de.numaria.application.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public class GsonHelper {

    public static final Gson PRETTY_GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
}
