package de.numaria.application.config;

import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 02.08.21
 */
@Getter
public class ModelBuildApplicationConfig {

    private String permission = "numaria.build";
    private String prefix = "§8[§bNumaria§8] §7";
    private String gameModeChangedNoPermission = "%prefix%Dein Spielmodus wurde auf §bSurvival §7geändert, weil du keine Rechte zum bauen hast.";
    private String buildModeOn = "%prefix%Der Baumodus wurde §aaktiviert§8.";
    private String buildModeOff = "%prefix%Der Baumodus wurde §cdeaktiviert§8.";
    private String noPermission = "%prefix%§cDazu hast du keine Berechtigung§8.";
}
