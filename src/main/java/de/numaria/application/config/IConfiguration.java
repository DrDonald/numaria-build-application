package de.numaria.application.config;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public interface IConfiguration<T> {

   IConfiguration<T> createIfNotExists(final Supplier<T> supplier);

   IConfiguration<T> load();

   IConfiguration<T> write(T type);

   IConfiguration<T> replaceKeys(String before, String after);

   IConfiguration<T> replaceKeysLocalGet(String before, Function<T, String> after);

   IConfiguration<T> save();

   boolean exists();

   /**
    * @return this instance if config exists. Otherwise it'll return {@link de.numaria.application.config.impl.EmptyConfiguration} for no further affects.
    */
   IConfiguration<T> continueIfExists();

   /**
    * @return this instance if config does not exist. Otherwise it'll return {@link de.numaria.application.config.impl.EmptyConfiguration} for no further affects.
    */
   IConfiguration<T> continueIfDoesntExists();

   T get();
}
