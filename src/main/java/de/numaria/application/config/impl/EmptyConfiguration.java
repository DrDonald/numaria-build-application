package de.numaria.application.config.impl;

import de.numaria.application.config.IConfiguration;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Use case: Can be used if
 * {@link IConfiguration#continueIfExists}
 *
 * @author Jasper Stritzke
 * @since 11.06.21
 */
public class EmptyConfiguration<T> implements IConfiguration<T> {

    @Override
    public IConfiguration<T> createIfNotExists(Supplier<T> supplier) {
        return this;
    }

    @Override
    public IConfiguration<T> load() {
        return this;
    }

    @Override
    public IConfiguration<T> replaceKeys(String before, String after) {
        return this;
    }

    @Override
    public IConfiguration<T> replaceKeysLocalGet(String before, Function<T, String> after) {
        return this;
    }

    @Override
    public IConfiguration<T> write(Object type) {
        return this;
    }

    @Override
    public IConfiguration<T> save() {
        return this;
    }

    @Override
    public boolean exists() {
        return false;
    }

    @Override
    public IConfiguration<T> continueIfExists() {
        return this;
    }

    @Override
    public IConfiguration<T> continueIfDoesntExists() {
        return this;
    }

    @Override
    public T get() {
        return null;
    }
}
