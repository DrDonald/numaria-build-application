package de.numaria.application.command;

import de.numaria.application.BuildApplication;
import lombok.AllArgsConstructor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Jasper Stritzke
 * @since 02.08.21
 */
@AllArgsConstructor
public class BuildCommand implements CommandExecutor {

    private final BuildApplication instance;

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)) return false;
        Player player = (Player) sender;

        if(!player.hasPermission(this.instance.getConfiguration().get().getPermission())) {
            player.sendMessage(this.instance.getConfiguration().get().getNoPermission());
            return false;
        }

        if(player.getGameMode() != GameMode.CREATIVE) {
            player.setGameMode(GameMode.CREATIVE);
            player.sendMessage(this.instance.getConfiguration().get().getBuildModeOn());

            player.playSound(player.getLocation(), Sound.PISTON_EXTEND, 1, 1);
            return false;
        }

        player.setGameMode(GameMode.SURVIVAL);
        player.sendMessage(this.instance.getConfiguration().get().getBuildModeOff());
        player.playSound(player.getLocation(), Sound.PISTON_RETRACT, 1, 1);
        return false;
    }
}
