package de.numaria.application.listener;

import de.numaria.application.BuildApplication;
import lombok.AllArgsConstructor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author Jasper Stritzke
 * @since 02.08.21
 */
@AllArgsConstructor
public class BuildSystemListener implements Listener {

    private final BuildApplication instance;

    @EventHandler
    public void handle(BlockBreakEvent event) {
        Player player = event.getPlayer();

        if (player.getGameMode() != GameMode.CREATIVE || !player.hasPermission(this.instance.getConfiguration().get().getPermission())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if (!event.getPlayer().hasPermission(this.instance.getConfiguration().get().getPermission())) {
            player.setGameMode(GameMode.SURVIVAL);
            player.sendMessage(this.instance.getConfiguration().get().getGameModeChangedNoPermission());
        }
    }
}
